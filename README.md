# Chaotic Soul Wordpress Theme

This theme was made by Bryan Veloso [[one]](https://bryanveloso.com/) [[two]](https://avalonstar.com/) and was [originally here](https://github.com/bryanveloso/chaoticsoul).

The theme is updated by [desbest](http://desbest.com).

![chaotic soul screenshot](https://i.imgur.com/dtPPKXP.png)
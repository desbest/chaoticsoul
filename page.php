<?php get_header(); ?>
    
    <div id="content" class="widecolumn">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
		<h2 class="title"><?php the_title(); ?></h2>
        <!-- desbest edit -->
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
        <center><img src="<?php echo $image[0]; ?>" style="max-width: 90%; max-height: 300px; clear: both; margin-left: auto; margin-right: auto;"></center>
        <?php endif; ?>
            <div class="entrytext">
				<?php the_content('<p class="serif">'.__('Read the rest of this page &raquo;', 'chaoticsoul').'</p>'); ?>
				<?php link_pages('<p><strong>'.__('Pages:', 'chaoticsoul').'</strong> ', '</p>', 'number'); ?>
            </div>
        </div>
    <?php endwhile; endif; ?>
	<?php edit_post_link(__('Edit this entry.', 'chaoticsoul'), '<p>', '</p>'); ?>
    
    <?php if ( comments_open() ) comments_template(); ?>
    </div>
    
<?php get_sidebar(); ?>

<?php get_footer(); ?>